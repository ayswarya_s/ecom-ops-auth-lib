const BaseHelper = require('ecom-base-lib').BaseHelper;
const SSOClient = require('../clients/sso-client');
const co = require('co');

class SSOAuthenticator extends BaseHelper {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
    this.ssoClient = new SSOClient(dependencies, config);
  }

  authenticate(request) {
    const me = this;
    const token = request.headers['sso-token'];
    if (token) {
      return co(function* () {
        try {
          let result = yield me.ssoClient.validateUser(token);
          return result;
        } catch (error) {
          console.log(error);
          throw error;
        }
      });
    }
  }

}

module.exports = SSOAuthenticator;
