'use strict'

const BaseServiceClient = require('ecom-base-lib').BaseServiceClient,
  co = require('co'),
  repoInfo = require('../../repo-info');


class SSOApiClient extends BaseServiceClient {
  constructor(dependencies, config, requestContext) {
    super(dependencies, config, requestContext);
  }

  validateUser(token) {
    const me = this;
    return co(function* () {
      try {
        let result = yield me._postMultiPartFormData('http://samsunglocal.samsung.com:8111/us/api/sup/tgt/validateaccesstkn', {}, {'token': token});
        return result;
      } catch (error) {
        console.log(error);
        throw error;
      }
    });
  }
}
module.exports = SSOApiClient
