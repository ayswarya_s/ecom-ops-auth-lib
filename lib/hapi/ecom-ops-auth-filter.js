'use strict';

// Load modules

const Hoek = require('hoek');
const Joi = require('joi');
const ExpressUserAgent = require('express-useragent');
const sessionCookie = 'remoteId';
const visitorIdCookie = 's_vi';
const profileIdCookie = 'prof_id';
const eComSessionCookie = 's_ecom_session';
const eComVisitorCookie = 'ecom_vi';
const targetMarketIdCookie = 'tmktid';
const targetMarketNameCookie = 'tmktname';
const accessorTypeCookie = 'taccessrtype';
const purchasePlanIdCookie = 'tppid';
const purchasePlanImageUrlCookie = 'tlgimg';
const purchaseFlowShoppingCartIdCookie = 's_ecom_pfscid';
const drSessionToken = 'DR_SESSION_TOKEN';
const webJWTTokenCookie = 's_ecom_jwt';
const headerStrings = require('ecom-base-lib').HttpHeaders;
const uuid = require('uuid');
const constants = require('../common/constants');
const enums = require('../common/enum');
const errorCodes = require('../errors/auth-errors');
const IdentityJSONToken = require('ecom-base-lib').authz.IdentityJSONToken;
const SsoClient = require('../clients/sso-api-client');
const GatewaySchema = require('../schema').GatewaySchema;
const _ = require('lodash');
const Boom = require('boom');
const co = require('co');

// Declare internals

const internals = {};

exports.register = function (server, options, next) {
  server.auth.scheme('ecom-ops-auth', internals.implementation);
  next();
};

internals.schema = Joi.object({dependencies: Joi.any().required(), config: Joi.any().required()});

exports.register.attributes = {
  pkg: require('../../package.json')
};

internals.implementation = function (server, options) {
  /*eslint-disable no-inner-declarations*/
  try {
    function _fillCredentialsWithEPPInfo (objTarget, objSource) {
      if (objSource.purchase_plan_id) {
        objTarget.purchase_plan_id = objSource.purchase_plan_id;
      }

      if (objSource.target_mkt_id) {
        objTarget.target_mkt_id = objSource.target_mkt_id;
      }

      if (objSource.target_mkt_name) {
        objTarget.target_mkt_name = objSource.target_mkt_name;
      }

      if (objSource.purchase_plan_image_url) {
        objTarget.purchase_plan_image_url = objSource.purchase_plan_image_url;
      }

      // Adding the check for null to make sure to reset the accessor_type based on SSO Response
      if (objSource.accessor_type || objSource.accessor_type === null) {
        objTarget.accessor_type = objSource.accessor_type;
      }

      if(objSource.email_invitation_address){
        objTarget.email_invitation_address = objSource.email_invitation_address;
      }

      if(objSource.email_invitation_pin){
        objTarget.email_invitation_pin = objSource.email_invitation_pin;
      }

      if(objSource.click_id){
        objTarget.click_id = objSource.click_id;
      }
    }

    function _checkAndHandleWrappedQuotesForCredentials (credentials, reply, privateKey, ipAddress) {
      return co(function * () {
        yield _createTokenAndSetCookie(credentials, reply, privateKey, ipAddress);
        return credentials;
      });
    }

    function _checkAndRemoveWrappedQuotes (input) {
      if (input && input.length > 2) {
        let sanitizedInput = input.replace(/[\\*"\\*']/g, '');
        if (sanitizedInput !== input) {
          return {
            isSanitized: true,
            output: sanitizedInput
          };
        }
      }
      return { isSanitized: false,
        output: input
      };
    }

    function _getCredentialsFromWebJWT (token) {
      let credentials = {
        is_authenticated: token.s_remote_id != null,
        s_remote_id: token.s_remote_id || null,
        visitor_id: token.visitor_id || null,
        identity_id: token.profile_id || null,
        ecom_session_id: token.ecom_session_id || null,
        purchase_flow_id: token.purchase_flow_id || null,
        shopping_cart_id: token.shopping_cart_id || null
      };

      _fillCredentialsWithEPPInfo(credentials, token);

      return credentials;
    }

    function _base64Decode (base64EncodedCookie) {
      let buf = Buffer.from(base64EncodedCookie, 'base64');
      return buf.toString('');
    }

    function _combineHeaderAndCookie (request) {
      let headerCredential;
      if (request.headers['x-ecom-cookie-credentials']) {
        headerCredential = JSON.parse(request.headers['x-ecom-cookie-credentials']);
        if (headerCredential[eComSessionCookie]) {
          headerCredential[eComSessionCookie] = _base64Decode(headerCredential[eComSessionCookie]);
        }

        if (headerCredential[eComVisitorCookie]) {
          headerCredential[eComVisitorCookie] = _base64Decode(headerCredential[eComVisitorCookie]);
        }
      }

      return headerCredential ? _.defaults(headerCredential, request.state) : request.state;
    }

    function _getCredentialsFromCookies (request, reply) {
      let credentials;
      let cookieCollection = _combineHeaderAndCookie(request);
      let visitorId = cookieCollection[eComVisitorCookie] || cookieCollection[visitorIdCookie] || null;
      if (!visitorId) {
        // If the s_vi cookie is not there, set the ecom one.
        visitorId = 'ecom|' + uuid.v4();
        // reply.state(eComVisitorCookie, visitorId);
      }
      let sessionToken = cookieCollection[sessionCookie] || null;
      let profileId = cookieCollection[profileIdCookie] || null;
      if (sessionToken || profileId) {
        if (!sessionToken) {
          profileId = null;
        } else if (!profileId) {
          sessionToken = null;
        }
      }
      let eComSessionId = cookieCollection[eComSessionCookie] || null;
      if (!eComSessionId) {
        eComSessionId = uuid.v4();
        reply.state(eComSessionCookie, eComSessionId);
      }
      credentials = {is_authenticated: sessionToken != null, s_remote_id: sessionToken, visitor_id: visitorId, identity_id: profileId, ecom_session_id: eComSessionId};

      if (cookieCollection[targetMarketNameCookie]) {
        credentials.target_mkt_name = cookieCollection[targetMarketNameCookie];
      }

      if (cookieCollection[purchasePlanImageUrlCookie]) {
        credentials.purchase_plan_image_url = cookieCollection[purchasePlanImageUrlCookie];
      }

      if (cookieCollection[accessorTypeCookie]) {
        credentials.accessor_type = cookieCollection[accessorTypeCookie];
      }

      if (cookieCollection[drSessionToken]) {
        credentials.dr_session_token = cookieCollection[drSessionToken];
      }

      if (cookieCollection[purchasePlanIdCookie]) {
        credentials.tpp_id = cookieCollection[purchasePlanIdCookie];
      }

      if (cookieCollection[targetMarketIdCookie]) {
        credentials.target_mkt_id = cookieCollection[targetMarketIdCookie];
      }

      if (cookieCollection[purchaseFlowShoppingCartIdCookie] && typeof (cookieCollection[purchaseFlowShoppingCartIdCookie]) === 'string') {
        let parts = cookieCollection[purchaseFlowShoppingCartIdCookie].split('|');
        if (parts.length === 2) {
          credentials.purchase_flow_id = parts[0];
          credentials.shopping_cart_id = parts[1];
        }
      }

      return credentials;
    }

    function _createTokenAndSetCookie (credentials, reply, privateKey, ipAddress, requestContext) {
      return co(function * () {
        let token = new IdentityJSONToken({
          first_name: credentials.first_name,
          last_name: credentials.last_name,
          user_id: credentials.user_id,
          email: credentials.email
        });

        // yield _fillEPPCredentialsFromSSO(credentials, ipAddress, requestContext);
        // options.dependencies.logger.log('log', { type: '_fillEPPCredentialsFromSSO response', credentials});
        // _fillCredentialsWithEPPInfo(token, credentials);
        // options.dependencies.logger.log('log', { type: '_fillCredentialsWithEPPInfo response', token});
        const jwtToken = token.sign(privateKey.key, {algorithm: privateKey.algo});
        reply.state(webJWTTokenCookie, jwtToken, {encoding: 'base64'});
      });
    }

    function _isEduDomain(credentials) {
      const eduEppInfo = options.config.eduEppInfo || {};
      const eduPurchasePlanId = eduEppInfo.purchasePlanId || constants.EduPurchasePlanId;
      const eduTargetMarketId = eduEppInfo.targetMarketId || constants.EduTargetMarketId;
      return (credentials.accessor_type &&
              (credentials.accessor_type.toLowerCase() === constants.EmailDomain.toLowerCase()) &&
              (credentials.tpp_id === eduPurchasePlanId) &&
              (credentials.target_mkt_id === eduTargetMarketId));
    }

    function _fillEPPCredentialsFromSSO(credentials, ipAddress, requestContext) {
      let eppCredentials;
      let eppVisitorCredentials;
      let credentialsToUse = { accessor_type: null };
      const ssoAccessor = options.dependencies.ssoAccessorFactory.createInstance(options.dependencies, options.config.sso, requestContext);

      return co(function* () {
        options.dependencies.logger.log('info', { scope: 'ecom-auth-filter', method: '_fillEPPCredentialsFromSSO', message: '_fillEPPCredentialsFromSSO', ipAddress: ipAddress, credentials: credentials });
        // Validate the session for referralUrl cases and for student discounts. Student discounts applies for users with .edu in their email domain
        if (credentials.accessor_type === constants.ReferralUrl || _isEduDomain(credentials)) {
          if (credentials.dr_session_token) {
            let ipAddressMatch = true;
            let isValidEPPUser = true;

            // EPP ip verification
            eppVisitorCredentials = yield ssoAccessor.validateVisitorSession(credentials.dr_session_token);
            options.dependencies.logger.log('log', { type: 'validateVisitorSession response', clientIP: ipAddress, eppClientIP: eppVisitorCredentials.client_ip, eppVisitorCredentials: eppVisitorCredentials, tppId : credentials.tpp_id, credentials: credentials });
            eppVisitorCredentials.accessor_type = _isEduDomain(credentials) ? constants.EmailDomain : eppVisitorCredentials.accessor_type;

            if (ipAddress !== eppVisitorCredentials.client_ip) {
              ipAddressMatch = false;
              options.dependencies.logger.log('error', { type: '_fillEPPCredentialsFromSSO IP address mismatch', clientIP: ipAddress, eppClientIP: eppVisitorCredentials.client_ip, eppVisitorCredentials: eppVisitorCredentials, tppId : credentials.tpp_id });
            }

            // EPP shopper id protection
            if (credentials.s_remote_id != null) {
              eppCredentials = yield ssoAccessor.validateSessionV2(credentials.s_remote_id, eppVisitorCredentials.shopper_id);
              eppVisitorCredentials.email_invitation_address = eppCredentials.email_invitation_address;
              eppVisitorCredentials.email_invitation_pin = eppCredentials.email_invitation_pin;
              options.dependencies.logger.log('info', { scope: 'ecom-auth-filter', method: '_fillEPPCredentialsFromSSO', message: 'validateSessionV2 referral url', ipAddress: ipAddress, credentials: credentials, eppCredentials: eppCredentials });
              if (!_isEduDomain(credentials) && (eppCredentials.epp_verified !== 'Y')) {
                isValidEPPUser = false;
                options.dependencies.logger.log('error', { type: '_fillEPPCredentialsFromSSO not a EPP verified user', eppVerified: eppCredentials.epp_verified, eppCredentials: eppCredentials, tppId : credentials.tpp_id });
              }
            }

            if (ipAddressMatch && isValidEPPUser) {
              credentialsToUse = eppVisitorCredentials;
            }
          }
        } else {
          // EPP Partners case
          if (credentials.s_remote_id != null) {
            eppCredentials = yield ssoAccessor.validateSessionV2(credentials.s_remote_id);
            options.dependencies.logger.log('info', { scope: 'ecom-auth-filter', method: '_fillEPPCredentialsFromSSO', message: 'validateSessionV2', ipAddress: ipAddress, credentials: credentials, eppCredentials: eppCredentials });
            if (eppCredentials.epp_verified === 'Y') {
              credentialsToUse = eppCredentials;
            } else {
              options.dependencies.logger.log('error', { type: '_fillEPPCredentialsFromSSO SSO returns epp_verified === N', eppCredentials: eppCredentials, tppId : credentials.tpp_id, credentials });
            }
          }
        }
        if(credentials.purchase_plan_id && credentialsToUse.epp_verified !== 'Y'){
          options.dependencies.logger.log('info', { scope: 'ecom-auth-filter', method: '_fillEPPCredentialsFromSSO', logType: 'event', eventType: 'SSOEppUserVerificationMismatch', ipAddress: ipAddress, credentials: credentials, ssoCredentials: credentialsToUse });
        }
        _fillCredentialsWithEPPInfo(credentials, credentialsToUse);
      });
    }

    function _getDeviceType (userAgent) {
      let deviceType = constants.DefaultDeviceType;
      _.forEach(enums.DeviceType, (type) => {
        if (userAgent[`is${type}`]) {
          deviceType = type;
          return false;
        }
      });
      return deviceType;
    }

    const results = Joi.validate(options, internals.schema);
    Hoek.assert(!results.error, results.error);

    const settings = results.value;

    const scheme = {
      authenticate: function (request, reply) {
        return co(function * () {
          try {
            if (request.path.toLowerCase().includes('documentation') ||
              request.path.toLowerCase().includes('swagger.json') ||
              (request.headers[constants.RefererHeader] || '').toLowerCase().includes('documentation')) {
              return reply.continue({credentials: {}});
            }

            const authzToken = request.headers['x-ecom-ops-jwt'];
            const ssoToken = request.headers['remote-id'];
            const authType = request.headers['ops-user-login'];
            let credentials;
            let ua = request.headers['user-agent'];
            let userAgent = ExpressUserAgent.parse(ua || '');

            let trueClientIP = request.headers['true-client-ip'];
            let clientIP = request.headers['client-ip'];
            let xForwardedFor = request.headers['x-forwarded-for'];
            let ip = trueClientIP || clientIP || xForwardedFor;
            if (!ip && request.info) {
              ip = request.info.remoteAddress || null;
            }
            options.dependencies.logger.log('info', { scope: 'ecom-auth-filter', method: 'authenticate', message: 'client ip', 'true-client-ip': trueClientIP, 'client-ip': clientIP, 'x-forwarded-for': xForwardedFor});

            let clientIdentity = {
              ipAddress: ip,
              port: request.info ? request.info.remotePort : null,
              ua,
              detectedUA: userAgent.browser,
              os: userAgent.os,
              platform: userAgent.platform,
              deviceType: _getDeviceType(userAgent)
            };
            let decodedCookie = _base64Decode(authzToken);
            if (decodedCookie) {
              // JWT code path

              const token = IdentityJSONToken.verify(decodedCookie, 'secretkey');
              if (!token) {
                if (authType == 'SSO') {
                  if (ssoToken) {
                    return reply(Boom.unauthorized(null, 'ecom-sso-auth'));
                  }
                  return reply.redirect("/app/login");
                } else {
                  return reply({error: 'login to knox'});
                }
              }

              credentials = {
                user_id: token.user_id,
                first_name: token.first_name,
                last_name: token.last_name,
                email: token.email
              };


              clientIdentity.clientType = enums.ClientType.MobileApp;
            } else {
              return reply("Missing auth token");
            }

            credentials = yield _checkAndHandleWrappedQuotesForCredentials(credentials, reply, {key: 'secretkey', algo: 'HS256'}, clientIdentity.ipAddress);
            credentials.clientIdentity = clientIdentity;
            request.headers['x-ecom-ops-internal-credentials'] = JSON.stringify(credentials);
            return reply.continue({credentials: credentials});
          } catch(err) {
            options.dependencies.logger.log('error', {requestId: request.headers ? request.headers['x-request-id'] : null, type: 'ecom-auth-filter Authenticate Error', errorStack: err.stack, errorMsg: err.message, state: request.state, headers: request.headers});
            return reply(Boom.badRequest(err.message));
          }
        });
      }
    };
    return scheme;
  } catch(ex) {
    options.dependencies.logger.log('error', { type: 'ecom-auth-filter Implementation error', errorStack: ex.stack, errorMsg: ex.message });
    throw ex;
  }
};
